import pickle
import os
import argparse
import numpy as np
from cs231n.data_utils import load_CIFAR10
from cs231n.classifiers.neural_net import TwoLayerNet

def get_CIFAR10_data(num_training=49000, num_validation=1000, num_test=1000):
	"""	
	Load the CIFAR-10 dataset from disk and perform preprocessing to prepare
	it for the two-layer neural net classifier. These are the same steps as
	we used for the SVM, but condensed to a single function.  
    	"""
	# Load the raw CIFAR-10 data
	cifar10_dir = 'cs231n/datasets/cifar-10-batches-py'
	
	X_train, y_train, X_test, y_test = load_CIFAR10(cifar10_dir)

    	# Subsample the data
	mask = list(range(num_training, num_training + num_validation))
	X_val = X_train[mask]
	y_val = y_train[mask]
	mask = list(range(num_training))
	X_train = X_train[mask]
	y_train = y_train[mask]
	mask = list(range(num_test))
	X_test = X_test[mask]
	y_test = y_test[mask]
	
	# Normalize the data: subtract the mean image
	mean_image = np.mean(X_train, axis=0)
	X_train -= mean_image
	X_val -= mean_image
	X_test -= mean_image

	# Reshape data to rows
	X_train = X_train.reshape(num_training, -1)
	X_val = X_val.reshape(num_validation, -1)
	X_test = X_test.reshape(num_test, -1)

	return X_train, y_train, X_val, y_val, X_test, y_test

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("--lr", help="learning rate", type=float)
	parser.add_argument("--rs", help="regularization strength", type=float)
	parser.add_argument("--it", help="iterations", type=int)
	parser.add_argument("--bs", help="batch size", type=int)
	args = parser.parse_args()

	# Cleaning up variables to prevent loading data multiple times (which may cause memory issue)
	try:
   		del X_train, y_train
   		del X_test, y_test
   		print('Clear previously loaded data.')
	except:
   		pass

	# Invoke the above function to get our data.
	X_train, y_train, X_val, y_val, X_test, y_test = get_CIFAR10_data()
	#print(X_train.shape, y_train.shape, X_val.shape, y_val.shape, X_test.shape, y_test.shape)

	input_size = 32 * 32 * 3
	hidden_size = 50
	num_classes = 10
	
	net = TwoLayerNet(input_size, hidden_size, num_classes)
	if os.path.isfile('./model_nn_w1.p'):
		net.params['W1'] = pickle.load( open( "model_nn_w1.p", "rb" ) )
		net.params['b1'] = pickle.load( open( "model_nn_b1.p", "rb" ) )
		net.params['W2'] = pickle.load( open( "model_nn_w2.p", "rb" ) )
		net.params['b2'] = pickle.load( open( "model_nn_b2.p", "rb" ) )
	else:
		std=1e-4
		net.params['W1'] = std * np.random.randn(input_size, hidden_size)
		net.params['b1'] = np.zeros(hidden_size)
		net.params['W2'] = std * np.random.randn(hidden_size, num_classes)
		net.params['b2'] = np.zeros(num_classes)

	# Train the network
	stats = net.train(X_train, y_train, X_val, y_val, num_iters=args.it, batch_size=args.bs,
                        learning_rate=args.lr, learning_rate_decay=0.95, reg=args.rs)
	pickle.dump( net.params['W1'], open( "model_nn_w1.p", "wb" ) )
	pickle.dump( net.params['b1'], open( "model_nn_b1.p", "wb" ) )
	pickle.dump( net.params['W2'], open( "model_nn_w2.p", "wb" ) )
	pickle.dump( net.params['b2'], open( "model_nn_b2.p", "wb" ) )
	
	# TODO: Remove it later; kept it for just checking
	test_acc = (net.predict(X_test) == y_test).mean()
	print('Test accuracy: ', test_acc)
