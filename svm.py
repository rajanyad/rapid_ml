import pickle
import os
import argparse
import numpy as np
from cs231n.data_utils import load_CIFAR10
from cs231n.classifiers import LinearSVM


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("--lr", help="learning rate", type=float)
	parser.add_argument("--rs", help="regularization strength", type=float)
	parser.add_argument("--it", help="iterations", type=int)
	parser.add_argument("--bs", help="batch size", type=int)
	args = parser.parse_args()

	# Load the raw CIFAR-10 data.
	cifar10_dir = 'cs231n/datasets/cifar-10-batches-py'

	# Cleaning up variables to prevent loading data multiple times (which may cause memory issue)
	try:
   		del X_train, y_train
   		del X_test, y_test
   		print('Clear previously loaded data.')
	except:
   		pass

	X_train, y_train, X_test, y_test = load_CIFAR10(cifar10_dir)

	# Preprocessing: reshape the image data into rows
	X_train = np.reshape(X_train, (X_train.shape[0], -1))
	X_test = np.reshape(X_test, (X_test.shape[0], -1))

	mean_image = np.mean(X_train, axis=0)
	# Subtract the mean image from train and test data
	X_train -= mean_image
	X_test -= mean_image
	
	# Append the bias dimension of ones (i.e. bias trick) so that our SVM
	# only has to worry about optimizing a single weight matrix W.
	X_train = np.hstack([X_train, np.ones((X_train.shape[0], 1))])
	X_test = np.hstack([X_test, np.ones((X_test.shape[0], 1))])
	#print(X_train.shape, X_test.shape)
	
	svm = LinearSVM()
	
	if os.path.isfile('./model_svm.p'):
		svm.W = pickle.load( open( "model_svm.p", "rb" ) )
	else:
		num_train, dim = X_train.shape
		num_classes = np.max(y_train) + 1
		svm.W = 0.001 * np.random.randn(dim, num_classes)

	svm.train(X_train, y_train, learning_rate=args.lr, reg=args.rs, num_iters=args.it, batch_size=args.bs)
	pickle.dump( svm.W, open( "model_svm.p", "wb" ) )	
	
	# TODO: Remove it later; kept it for just checking
	y_test_pred = svm.predict(X_test)
	test_accuracy = np.mean(y_test == y_test_pred)
	print('Accuracy: %f' % test_accuracy)
