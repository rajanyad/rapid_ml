#include <bits/stdc++.h>
using namespace std;

int main ()
{
	// Delete previously saved model if it exists
	string rm_str = "rm model_svm.p";
	const char *rm_command = rm_str.c_str();
	system(rm_command);
	
	for(int loop = 0; loop < 10; loop++) {

		cout<< "Running loop: " << loop+1 << endl;
    		
		string str = "python svm.py --lr .0000001 --rs 25000 --it 100 --bs 300";
    		// Convert string to const char * as system requires parameter of type const char *
    		const char *command = str.c_str();
    		system(command);
	}
 
    return 0;
}
