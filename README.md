### To compile and run:

#### SVM
$ c++ -o rapid_wrapper_svm rapid_wrapper_svm.cpp

$ ./rapid_wrapper_svm 


#### Neural Network
$ c++ -o rapid_wrapper_nn rapid_wrapper_nn.cpp

$ ./rapid_wrapper_nn 